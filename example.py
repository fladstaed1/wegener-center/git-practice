"""Do something else"""
import os
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def fit(x, y, *x_fit):
    params = np.polyfit(x, y, 1)
    p = np.poly1d(params)
    # y_fit = p(x_fit)
    if x_fit:
        y_fit = p(x_fit)
    else:
        y_fit = p(x)
    return y_fit


begin = "2000-01"
end = "2010-10"
title = "Timeseries"
T = "t"

df = "TAG_Datensatz_18700101_20220930_Graz.csv"

df = pd.read_csv(df, parse_dates=["time"], index_col="time")

print("Calculating anomalies")

df_clim = df["1991-01":"2020-12"]
clim = df_clim.groupby(df_clim.index.month).mean()
clim.index.name = "month"

monthly_means = df.groupby([df.index.year, df.index.month]).mean()
monthly_means.index.names = ["year", "month"]

anom = monthly_means - clim

years = anom.index.get_level_values(0).astype(str)
months = anom.index.get_level_values(1).astype(str)
dates = pd.to_datetime(years + "-" + months + "-01")
anom = anom.set_index(dates)


print("Calculating trend")

x = np.arange(df[T].values[~np.isnan(df[T].values)].shape[0])
y = df[T].values[~np.isnan(df[T].values)]

# params = np.polyfit(x, y, 1)
x_fit = np.arange(df[T].shape[0])
# p = np.poly1d(params)
# y_fit = p(x_fit)
y_fit = fit(x, y, x_fit)
y_fit = y_fit[0, :]
trend = df[T] * 0 + y_fit

print("Calculating the number of hot and cold days")

n_hot_days = (
    df["tmax"][df["tmax"] > 30].groupby([df["tmax"][df["tmax"] > 30].index.year]).size()
)
n_cold_days = (
    df["tmin"][df["tmin"] < 0].groupby([df["tmax"][df["tmin"] < 0].index.year]).size()
)

trend_hot = fit(n_hot_days.index, n_hot_days.values)
trend_cold = fit(n_cold_days.index, n_cold_days.values)

df_hot = pd.DataFrame(
    data=np.column_stack((n_hot_days.values, trend_hot)),
    index=n_hot_days.index.values,
    columns=["n", "trend"],
)
df_hot.index.name = "time"
df_cold = pd.DataFrame(
    data=np.column_stack((n_cold_days.values, trend_cold)),
    index=n_cold_days.index.values,
    columns=["n", "trend"],
)
df_cold.index.name = "time"

years = df_hot.index.astype(str)
dates = pd.to_datetime(years)
df_hot = df_hot.set_index(dates)

years = df_cold.index.astype(str)
dates = pd.to_datetime(years)
df_cold = df_cold.set_index(dates)

# hot_index_values = n_hot_days.index.values
# trend_hot = pd.DataFrame(data = trend_hot, index = hot_index_values, columns = ["n"])
# trend_hot.index.name = "time"
# n_hot_days = pd.DataFrame(data = n_hot_days.values, index = n_hot_days.index, columns=["n"])

# cold_index_values = n_cold_days.index.values
# trend_cold = pd.DataFrame(data = trend_cold, index = cold_index_values, columns = ["n"])
# trend_cold.index.name = "time"
# n_cold_days = pd.DataFrame(data = n_cold_days.values, index = n_cold_days.index, columns=["n"])


print("Plotting timeseries.")

df = df[begin:end]
anom = anom[begin:end]
trend = trend[begin:end]

fig, ax = plt.subplots()
ax.plot(df.index, df[T])
ax.set_ylabel("T / K")
ax.set_xlabel("time")
ax.plot(df.index, trend.values)
ax.set(title=title)
plt.show()

# n_hot_days = n_hot_days.loc[begin:end]
# n_cold_days = n_cold_days.loc[begin:end]
# trend_hot = trend_hot.loc[begin:end]
# trend_cold = trend_cold.loc[begin:end]

begin = datetime.strptime(begin, "%Y-%m").date()
end = datetime.strptime(end, "%Y-%m").date()

df_hot = df_hot[begin:end]
df_cold = df_cold[begin:end]

fig, ax = plt.subplots(2)
ax[0].plot(df_hot["n"], "x")
ax[0].plot(df_hot["trend"])
ax[0].set(title="Number of hot and cold days", ylabel="Number of hot days")
ax[1].plot(df_cold["n"], "x")
ax[1].plot(df_cold["trend"])
ax[1].set(xlabel="Years", ylabel="Number of cold days")
plt.show()

# fig, ax = plt.subplots(2)
# ax[0].plot(n_hot_days, "x")
# ax[0].plot(n_hot_days.index, trend_hot)
# ax[0].set(title="Number of hot and cold days", ylabel="Number of hot days")
# ax[1].plot(n_cold_days, "x")
# ax[1].plot(n_cold_days.index, trend_cold)
# ax[1].set(xlabel="Years", ylabel="Number of cold days")
# plt.show()
print("This is a meaningful change")